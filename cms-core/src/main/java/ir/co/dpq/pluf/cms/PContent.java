package ir.co.dpq.pluf.cms;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.google.gson.annotations.SerializedName;

@Entity(name = "content")
@Table(name = "content")
public class PContent implements Serializable {

	private static final long serialVersionUID = 8251659198713260263L;

	@Id
	@Column(name = "id", nullable = false)
	private long id;

	@Column(name="name", nullable = false, unique = true)
	private String name;

	@Column(nullable = true)
	private String title;

	@Column(nullable = true)
	private String description;

	@Column(name = "mime_type")
	private String mimeType;

	@Column(name = "file_name")
	private String fileName;

	@Column(name = "file_size")
	private long fileSize;

	private int downloads;

	@Column(name = "creation_dtime")
	@SerializedName("creation_dtime")
	@Temporal(TemporalType.DATE)
	private Date creation;

	@Column(name = "modif_dtime")
	@SerializedName("modif_dtime")
	@Temporal(TemporalType.DATE)
	private Date modification;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public int getDownloads() {
		return downloads;
	}

	public void setDownloads(int downloads) {
		this.downloads = downloads;
	}

	public Date getCreation() {
		return creation;
	}

	public void setCreation(Date creation) {
		this.creation = creation;
	}

	public Date getModification() {
		return modification;
	}

	public void setModification(Date modification) {
		this.modification = modification;
	}

	public Map<String, Object> toMap() {
		HashMap<String, Object> map = new HashMap<String, Object>();

		map.put("id", getId());
		map.put("name", getName());
		map.put("title", getTitle());
		map.put("description", getDescription());
		// map.put("file", file);

		return map;
	}

}
