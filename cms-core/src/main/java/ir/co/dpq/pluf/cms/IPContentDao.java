package ir.co.dpq.pluf.cms;

import java.io.File;

import ir.co.dpq.pluf.IModelDao;
import ir.co.dpq.pluf.IPCallback;

public interface IPContentDao extends IModelDao<PContent>{

	void getByName(String name, IPCallback<PContent> callback);

	void downloadContentValue(long id, IPCallback<File> callback);
	
	void updateContentValue(long id, IPCallback<PContent> callback);
	
	PContent getByName(String name);
	
	File downloadContentValue(long id);
	
	PContent updateContentValue(long id);
}
