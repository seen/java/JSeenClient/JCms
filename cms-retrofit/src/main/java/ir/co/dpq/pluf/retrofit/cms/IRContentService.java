package ir.co.dpq.pluf.retrofit.cms;

import java.util.Map;

import ir.co.dpq.pluf.cms.PContent;
import ir.co.dpq.pluf.retrofit.RPaginatorPage2;
import retrofit.Callback;
import retrofit.http.DELETE;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.QueryMap;

public interface IRContentService {

	@FormUrlEncoded
	@POST("/api/cms/new")
	PContent create(@FieldMap Map<String, Object> map);

	@FormUrlEncoded
	@POST("/api/cms/new")
	void create(@FieldMap Map<String, Object> map, Callback<PContent> callback);

	@GET("/api/cms/{contentId}")
	PContent get(@Path("contentId") long contentId);
	
	@GET("/api/cms/{name}")
	PContent get(@Path("name") String contentName);

	@GET("/api/cms/{contentId}")
	void get(@Path("contentId") long contentId, Callback<PContent> callback);

	@GET("/api/cms/{name}")
	void get(@Path("name") String contentName, Callback<PContent> callback);

	@FormUrlEncoded
	@POST("/api/cms/{contentId}")
	PContent update(@Path("contentId") long contentId, @FieldMap Map<String, Object> map);

	@FormUrlEncoded
	@POST("/api/cms/{contentId}")
	void update(@Path("contentId") long contentId, @FieldMap Map<String, Object> map, Callback<PContent> callback);

	@DELETE("/api/cms/{contentId}")
	PContent delete(@Path("contentId") long id);

	@DELETE("/api/cms/{contentId}")
	void delete(@Path("contentId") long id, Callback<PContent> callback);

	@GET("/api/cms/find")
	RPaginatorPage2<PContent> find(@QueryMap Map<String, Object> params);

	@GET("/api/cms/find")
	void find(@QueryMap Map<String, Object> params, Callback<RPaginatorPage2<PContent>> callback);
	
	// TODO: hadi 1396-05: download content and update file of content

}
