package ir.co.dpq.pluf.retrofit.cms;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import ir.co.dpq.pluf.IPCallback;
import ir.co.dpq.pluf.IPPaginatorPage;
import ir.co.dpq.pluf.PException;
import ir.co.dpq.pluf.PPaginatorParameter;
import ir.co.dpq.pluf.cms.IPContentDao;
import ir.co.dpq.pluf.cms.PContent;
import ir.co.dpq.pluf.retrofit.RPaginatorPage2;
import ir.co.dpq.pluf.retrofit.Util;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RContentDao implements IPContentDao {

	private IRContentService contentService;

	public IRContentService getRService() {
		return contentService;
	}

	public void setRService(IRContentService contentService) {
		this.contentService = contentService;
	}

	/**
	 * Wraps an {@link IPCallback} to a {@link Callback} and return wrapped
	 * callback
	 * 
	 * @param callback
	 *            {@link IPCallback}
	 * @return {@link Callback}
	 */
	private static Callback<PContent> wrapToCallback(final IPCallback<PContent> callback) {
		Callback<PContent> cb = new Callback<PContent>() {
			public void success(PContent t, Response response) {
				callback.success(t);
			}

			public void failure(RetrofitError error) {
				callback.failure(new PException(error.getMessage(), error));
			}
		};
		return cb;
	}

	public void create(PContent content, IPCallback<PContent> callback) {
		contentService.create(content.toMap(), wrapToCallback(callback));
	}

	public void get(long id, IPCallback<PContent> callback) {
		contentService.get(id, wrapToCallback(callback));
	}

	public void update(PContent content, IPCallback<PContent> callback) {
		contentService.update(content.getId(), content.toMap(), wrapToCallback(callback));
	}

	public void delete(long id, IPCallback<PContent> callback) {
		contentService.delete(id, wrapToCallback(callback));
	}

	public void find(PPaginatorParameter param, final IPCallback<IPPaginatorPage<PContent>> callback) {
		Map<String, Object> paramMap;
		if (param == null) {
			paramMap = new HashMap<String, Object>();
		} else {
			paramMap = Util.toRObject(param).toMap();
		}
		contentService.find(paramMap, new Callback<RPaginatorPage2<PContent>>() {

			public void success(RPaginatorPage2<PContent> result, Response response) {
				callback.success(result);
			}

			public void failure(RetrofitError error) {
				callback.failure(new PException(error.getMessage(), error));
			}
		});
	}

	public PContent create(PContent content) {
		return contentService.create(content.toMap());
	}

	public PContent get(long id) {
		return contentService.get(id);
	}

	public PContent update(PContent content) {
		return contentService.update(content.getId(), content.toMap());
	}

	public PContent delete(long id) {
		return contentService.delete(id);
	}

	public IPPaginatorPage<PContent> find(PPaginatorParameter param) {
		Map<String, Object> paramMap;
		if (param == null) {
			paramMap = new HashMap<String, Object>();
		} else {
			paramMap = Util.toRObject(param).toMap();
		}
		return contentService.find(paramMap);
	}

	public void getByName(String name, IPCallback<PContent> callback) {
		contentService.get(name, wrapToCallback(callback));
	}

	public void downloadContentValue(long id, IPCallback<File> callback) {
		// TODO Auto-generated method stub

	}

	public void updateContentValue(long id, IPCallback<PContent> callback) {
		// TODO Auto-generated method stub

	}

	public PContent getByName(String name) {
		return contentService.get(name);
	}

	public File downloadContentValue(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	public PContent updateContentValue(long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
