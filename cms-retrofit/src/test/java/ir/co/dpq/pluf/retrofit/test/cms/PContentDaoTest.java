package ir.co.dpq.pluf.retrofit.test.cms;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ir.co.dpq.pluf.PUserDaoRetrofit;
import ir.co.dpq.pluf.cms.IPContentDao;
import ir.co.dpq.pluf.retrofit.PErrorHandler;
import ir.co.dpq.pluf.retrofit.cms.IRContentService;
import ir.co.dpq.pluf.retrofit.cms.RContentDao;
import ir.co.dpq.pluf.retrofit.user.IRUserService;
import ir.co.dpq.pluf.test.cms.AbstractPContentDaoTest;
import ir.co.dpq.pluf.test.cms.TestCoreConstant;
import ir.co.dpq.pluf.user.IPUserDao;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class PContentDaoTest extends AbstractPContentDaoTest {

	IPContentDao contentDao;
	IPUserDao userDao;

	public PContentDaoTest() {
		CookieManager cookieManager = new CookieManager();
		cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
		CookieHandler.setDefault(cookieManager);

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder//
				.setDateFormat("yyyy-MM-dd HH:mm:ss");//
		Gson gson = gsonBuilder.create();

		RestAdapter restAdapter = new RestAdapter.Builder()
				// تعیین مبدل داده
				.setConverter(new GsonConverter(gson))
				// تعیین کنترل کننده خطا
				.setErrorHandler(new PErrorHandler())
				// تعیین آدرس سایت مورد نظر
				.setEndpoint(TestCoreConstant.END_POINT)
				// ایجاد یک نمونه
				.build();

		// ایجاد سرویس‌ها
		IRContentService contentService = restAdapter.create(IRContentService.class);
		RContentDao cntDao = new RContentDao();
		cntDao.setRService(contentService);
		contentDao = cntDao;
		
		IRUserService userSerivece = restAdapter.create(IRUserService.class);
		PUserDaoRetrofit usrDao = new PUserDaoRetrofit();
		usrDao.setUserService(userSerivece);
		userDao = usrDao;
	}

	@Override
	protected IPUserDao getUserDaoInstance() {
		return userDao;
	}

	@Override
	protected IPContentDao getPContentDaoInstance() {
		return contentDao;
	}
	
}
