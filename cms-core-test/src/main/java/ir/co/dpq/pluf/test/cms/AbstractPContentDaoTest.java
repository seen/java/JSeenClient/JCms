package ir.co.dpq.pluf.test.cms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Random;

import org.junit.Test;

import ir.co.dpq.pluf.IModelDao;
import ir.co.dpq.pluf.cms.IPContentDao;
import ir.co.dpq.pluf.cms.PContent;
import ir.co.dpq.pluf.test.AbstractModelDaoTest;

public abstract class AbstractPContentDaoTest extends AbstractModelDaoTest<PContent> {

	private IPContentDao contentDao;
	
	@Override
	public void initTest() {
		super.initTest();
		contentDao = getPContentDaoInstance();
	}
	
	protected abstract IPContentDao getPContentDaoInstance();
	
	@Override
	protected IModelDao<PContent> getModelDaoInstance() {
		return getPContentDaoInstance();
	}
	
	@Override
	protected PContent getNewModel() {
		Random rnd = new Random(System.currentTimeMillis());
		int num = rnd.nextInt();
		PContent cnt = new PContent();
		cnt.setName("content-" + num);
		cnt.setTitle("Content Title " + num);
		cnt.setDescription("Description of content-" + num);
		return cnt;
	}

	@Override
	protected void assertModel(PContent obj, PContent newObj) {
		assertEquals(obj.getName(), newObj.getName());
		assertEquals(obj.getTitle(), newObj.getTitle());
		assertEquals(obj.getDescription(), newObj.getDescription());
	}

	@Override
	protected long getId(PContent obj) {
		return obj.getId();
	}

	@Test
	public void getByNameTest(){
		PContent obj = getNewModel();
		PContent obj2 = contentDao.create(obj);
		assertNotNull(obj2);
		assertModel(obj, obj2);

		PContent obj3 = contentDao.getByName(obj2.getName());
		assertNotNull(obj3);
		assertModel(obj, obj3);
		
		modelCache.add(obj2);
	}
	
}
